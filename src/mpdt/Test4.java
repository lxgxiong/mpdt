package mpdt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

//从老数据库读取数据，清理后存进新数据库
public class Test4 {
	
	private Map<String, Tweet> map;
	private TweetDao dao;
	private List<Tweet> tweets;
	
	public static void main(String[] args) {
		long time = System.currentTimeMillis();
		Test4 t = new Test4();
		t.initList();
		t.cleanContent();
		System.out.println(t.tweets.size());
		
		Map<String, Tweet> map = new HashMap<String, Tweet>();
		
		for (Tweet tweet : t.tweets) {
			tweet.setContent(tweet.getContent().trim());
			if(tweet.getContent().length()<=10)
				continue;
			map.put(tweet.getContent(),tweet);
		}
		t.map=map;
		System.out.println(map.size());
		//t.dao.addtweetBatch(t.tweets);
		t.write();
		System.out.println(System.currentTimeMillis()-time);
	}
	
	public void write(){
        File f = new File("tweets1.txt");
        try {
			f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
        FileOutputStream fos;
		try {
			fos = new FileOutputStream("tweets1.txt", false);
    		PrintStream p = new PrintStream(fos);	
    		int count = 0;
    		System.out.println(count+"file created!");
    		
    		for (Tweet tweet : map.values()) {
    			count++;
//				p.println(count+"\t"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tweet.getCreationTime())+" "+tweet.getContent());
				p.println(tweet.getContent());

			}
    		
            p.close();
            
            System.out.println("Writing finished!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanContent(){
		for (Tweet tweet : tweets) {
			String str = tweet.getContent();
			str = Regex.VALID_MENTION_OR_LIST.matcher(str).replaceAll("");
            str = Regex.VALID_URL.matcher(str).replaceAll("");
            str = Pattern.compile("RT: ").matcher(str).replaceAll("");
            str = Regex.VALID_HASHTAG.matcher(str).replaceAll("");
            str = str.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
            str.trim();
			tweet.setContent(str);
		}
		
	}
	
	public void initList() {
		tweets = dao.select("select * from tweets_sample limit 1,100000");
	}
	
	
	public Test4() {
		map = new HashMap<String, Tweet>();
		dao = new TweetDao();
	}

	public Map<String, Tweet> getMap() {
		return map;
	}

	public void setMap(Map<String, Tweet> map) {
		this.map = map;
	}

	public TweetDao getDao() {
		return dao;
	}

	public void setDao(TweetDao dao) {
		this.dao = dao;
	}

	public List<Tweet> getTweets() {
		return tweets;
	}

	public void setTweets(List<Tweet> tweets) {
		this.tweets = tweets;
	}
	

	
	
	
}
