package mpdt;

import java.sql.Date;

public class Tweet {
	private long id;
	private int userId;
	private String username;
	private String content;
	private Date creationTime;
	private int favorite;
	private long replyToPostId;
	private String replyToUsername;
	private long retweetedFromPostId;
	private int retweetCount;
	private String retweetedFromUsername;
	private String placeURL;
	private Double latitude;
	private Double longitude;
	private String placeCountry;
	private String placeCountryCode;
	private String placeStreetAddress;
	private String placeGeometryType;
	private String placeName;
	private String placeFullName;
	private String placeId;
	private String source;
	private String json;
	private Date timeOfCrawl;
	private int crawledViaNewsMedia;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public int getFavorite() {
		return favorite;
	}

	public void setFavorite(int favorite) {
		this.favorite = favorite;
	}

	public long getReplyToPostId() {
		return replyToPostId;
	}

	public void setReplyToPostId(long replyToPostId) {
		this.replyToPostId = replyToPostId;
	}

	public String getReplyToUsername() {
		return replyToUsername;
	}

	public void setReplyToUsername(String replyToUsername) {
		this.replyToUsername = replyToUsername;
	}

	public long getRetweetedFromPostId() {
		return retweetedFromPostId;
	}

	public void setRetweetedFromPostId(long retweetedFromPostId) {
		this.retweetedFromPostId = retweetedFromPostId;
	}

	public int getRetweetCount() {
		return retweetCount;
	}

	public void setRetweetCount(int retweetCount) {
		this.retweetCount = retweetCount;
	}

	public String getRetweetedFromUsername() {
		return retweetedFromUsername;
	}

	public void setRetweetedFromUsername(String retweetedFromUsername) {
		this.retweetedFromUsername = retweetedFromUsername;
	}

	public String getPlaceURL() {
		return placeURL;
	}

	public void setPlaceURL(String placeURL) {
		this.placeURL = placeURL;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getPlaceCountry() {
		return placeCountry;
	}

	public void setPlaceCountry(String placeCountry) {
		this.placeCountry = placeCountry;
	}

	public String getPlaceCountryCode() {
		return placeCountryCode;
	}

	public void setPlaceCountryCode(String placeCountryCode) {
		this.placeCountryCode = placeCountryCode;
	}

	public String getPlaceStreetAddress() {
		return placeStreetAddress;
	}

	public void setPlaceStreetAddress(String placeStreetAddress) {
		this.placeStreetAddress = placeStreetAddress;
	}

	public String getPlaceGeometryType() {
		return placeGeometryType;
	}

	public void setPlaceGeometryType(String placeGeometryType) {
		this.placeGeometryType = placeGeometryType;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getPlaceFullName() {
		return placeFullName;
	}

	public void setPlaceFullName(String placeFullName) {
		this.placeFullName = placeFullName;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public Date getTimeOfCrawl() {
		return timeOfCrawl;
	}

	public void setTimeOfCrawl(Date timeOfCrawl) {
		this.timeOfCrawl = timeOfCrawl;
	}

	public int getCrawledViaNewsMedia() {
		return crawledViaNewsMedia;
	}

	public void setCrawledViaNewsMedia(int crawledViaNewsMedia) {
		this.crawledViaNewsMedia = crawledViaNewsMedia;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}

		if (obj instanceof Tweet) {
			Tweet tweet = (Tweet) obj;
			if (tweet.getContent() == this.getContent()) {
				return true;
			} 
			
			if (tweet.getContent().equals(this.getContent())) {
				return true;
			}
		}

		return false;
	}

}
