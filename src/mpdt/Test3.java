package mpdt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;


public class Test3 {

    public static void main(String[] args) {
    	

        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        java.sql.PreparedStatement pst = null;
        ResultSet rs1 = null;

        String url = "jdbc:mysql://localhost:3306/tt";
        String user = "root";
        String password = "319720";

        try {
            con = DriverManager.getConnection(url, user, password);
            st = con.createStatement();
            rs = st.executeQuery("SELECT date_format(timeOfCrawl, '%Y-%m-%d %T'), content from tweets_sample limit 100000");

            pst = con.prepareStatement("insert into batch_test(keyword) values(?)");
            
            File f = new File("tweets1.txt");
            try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
            FileOutputStream fos;
			try {
				fos = new FileOutputStream("tweets1.txt", false);
	    		PrintStream p = new PrintStream(fos);	
	    		int count = 0;
	    		System.out.println(count+"file created!");
	            while (rs.next()) {
	            	count++;
	            	String str = rs.getString(2);
	                str = Regex.VALID_MENTION_OR_LIST.matcher(str).replaceAll("");
	                str = Regex.VALID_URL.matcher(str).replaceAll("");
	                str = Pattern.compile("RT: ").matcher(str).replaceAll("");
	                str = Regex.VALID_HASHTAG.matcher(str).replaceAll("");
	                str = str.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
	                //p.println(count+"\t"+rs.getString(1)+" "+str);
	                
	            }
	            p.close();
	            
	            System.out.println("\n\nDone!!");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}


        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Test3.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                
                if(rs1 !=null){
                	rs1.close();
                }
                
                if (pst != null) {
                    pst.close();
                }
                
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
                
                

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Test3.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
        
        
    }
}