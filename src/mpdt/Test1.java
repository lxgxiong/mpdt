package mpdt;

import java.util.List;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class Test1 {
    public static void main(String[] args) {
//        if (args.length < 1) {
//            System.out.println("java twitter4j.examples.search.SearchTweets [query]");
//            System.exit(-1);
//        }
        Twitter twitter = new TwitterFactory().getInstance();
        try {
            Query query = new Query();
            query.setCount(3000);
            query.setQuery("hello");
            query.setLang("en");
            QueryResult result;
//            do {
                result = twitter.search(query);
                List<Status> tweets = result.getTweets();
                System.out.println(tweets.size());
//                for (Status tweet : tweets) {
//                    System.out.println("@" + tweet.getUser().getScreenName() + " - " + tweet.getText());
//                }
                System.out.println("@"+result.getTweets().get(0).getUser().getScreenName()+"-"+result.getTweets().get(0).getText());
                query.setMaxId(tweets.get(tweets.size()-1).getId());
                result = twitter.search(query);
                System.out.println("@"+result.getTweets().get(0).getUser().getScreenName()+"-"+result.getTweets().get(0).getText());
//            } while ((query = result.nextQuery()) != null);
//            System.exit(0);}
        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to search tweets: " + te.getMessage());
            System.exit(-1);
        }
    }
}
