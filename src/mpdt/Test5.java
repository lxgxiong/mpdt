package mpdt;

import java.util.HashMap;
import java.util.Map;


public class Test5 {
	public static void main(String[] args) {
		Tweet t1 = new Tweet();
		Tweet t2 = new Tweet();
		t1.setId(111);
		t2.setId(222);
		t1.setContent("111");
		t2.setContent("111");
		System.out.println(t1.equals(t2));
		
		Map<Tweet, Integer> map = new HashMap<Tweet, Integer>();
		map.put(t1, 1);
		System.out.println(map.containsKey(t1));
		System.out.println(map.containsKey(t2));
		
		map.put(t2, 1);
		System.out.println(map.size());
	}
}
