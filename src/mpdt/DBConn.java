package mpdt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConn {
	// 四个核心变量、六个方法
	private static Connection conn = null;// 连接数据库
	private static Statement stmt = null;// 发送SQL命令
	private static PreparedStatement pstmt = null;// 发送带参数的sql命令
	private static ResultSet rs = null;// 获得返回的数据集

	public static final String DRIVER="com.mysql.jdbc.Driver";
	public static final String URL = "jdbc:mysql://localhost:3306/";
	public static final String DBNAME = "tt";
	public static final String DBUSERNAME = "root";
	public static final String DBPASS =  "319720";
	
	public static Connection getConnection() {
		// step1:找驱动
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL + DBNAME,
					DBUSERNAME, DBPASS);
			// DatabaseMetaData dbmd = conn.getMetaData();
			// System.out.println("db name: " + dbmd.getDatabaseProductName());
			// System.out.println("tx: " + dbmd.supportsTransactions());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	/**
	 * Author:Allison 
	 * Date:2011-10-12 
	 * Description:执行RUD操作
	 */
	public static int exeIUD(String sqlText) {
		// step1:创建连接
		getConnection();
		// step2:判断连接
		if (conn != null) {
			try {
				// step3:定义statement对象
				stmt = conn.createStatement();
				// step4:执行sql命令
				int iRs = stmt.executeUpdate(sqlText);
				return iRs;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	/**
	 * Author:Allison 
	 * Date:2011-10-12 
	 * Description:执行RUD操作
	 */

	public static int exePreIUD(String sqlText, Object[] oParams) {
		// step1:创建连接
		getConnection();

		// step2：判断连接
		if (conn != null) {
			try {
				// conn.setAutoCommit(false);
				// step3:定义pstmt对象
				pstmt = conn.prepareStatement(sqlText);
				// step:传参
				for (int i = 0; i < oParams.length; i++) {
					pstmt.setObject(i + 1, oParams[i]);
				}
				// step5:执行sql命令
				int iRs = pstmt.executeUpdate();
				return iRs;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	/**
	 * Author:Allison 
	 * Date:2011-10-12 
	 * Description:执行select操作
	 */
	public static ResultSet exeR(String sqlText) {
		// step1:建立连接
		getConnection();
		// step2:判断连接
		if (conn != null) {
			try {
				// step3:建立stmt对象
				stmt = conn.createStatement();
				// step4:执行sql命令
				rs = stmt.executeQuery(sqlText);
				return rs;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Author:Allison 
	 * Date:2011-10-12 
	 * Description:执行select操作
	 */
	public static ResultSet exePreR(String sqlText, Object[] oParams) {
		// step1:建立连接
		getConnection();
		// step2:判断连接
		if (conn != null) {
			try {
				// step3:建立stmt对象
				pstmt = conn.prepareStatement(sqlText);
				// step4:循环参数
				for (int i = 0; i < oParams.length; i++) {
					pstmt.setObject(i + 1, oParams[i]);
				}
				// step5:执行sql命令
				rs = pstmt.executeQuery();
				return rs;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Author:Allison 
	 * Date:2011-10-12 
	 * Description:关闭四个核心变量
	 */
	public static void close() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (pstmt != null) {
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
