package mpdt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TweetDao {
	private static Connection conn = null;  
    private static PreparedStatement pstmt = null;  
      
    public void addtweetBatch(List<Tweet> list) {  
        conn = DBConn.getConnection();  
        try {  
            conn.setAutoCommit(false);  
            String sql = "insert into tweets("
            		+ "`id`,`userId`,`username`,`content`,`creationTime`,"
            		+ "`favorite`,`replyToPostId`,`replyToUsername`,`retweetedFromPostId`,"
            		+ "`retweetedFromUsername`,`retweetCount`,`latitude`,`longitude`,"
            		+ "`placeCountry`,`placeCountryCode`,`placeStreetAddress`,`placeURL`,"
            		+ "`placeGeometryType`,`placeName`,`placeFullName`,`placeId`,`source`,"
            		+ "`json`,`timeOfCrawl`,`crawledViaNewsMedia`)values(?,?,?,?,?,?,?,?,?,?,?,"
            		+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?)";  
            
            pstmt = conn.prepareStatement(sql);  
            for (Tweet tweet : list) { 
                pstmt.setLong(1, tweet.getId());
                pstmt.setLong(2, tweet.getUserId());
                pstmt.setString(3, tweet.getUsername());
                pstmt.setString(4, tweet.getContent());
                pstmt.setDate(5, tweet.getCreationTime());
                pstmt.setInt(6, tweet.getFavorite());
                pstmt.setLong(7, tweet.getReplyToPostId());
                pstmt.setString(8, tweet.getReplyToUsername());
                pstmt.setLong(9, tweet.getRetweetedFromPostId());
                pstmt.setString(10, tweet.getRetweetedFromUsername());
                pstmt.setInt(11, tweet.getRetweetCount());
                pstmt.setDouble(12, tweet.getLatitude());
                pstmt.setDouble(13, tweet.getLongitude());
                pstmt.setString(14, tweet.getPlaceCountry());
                pstmt.setString(15, tweet.getPlaceCountryCode());
                pstmt.setString(16, tweet.getPlaceStreetAddress());
                pstmt.setString(17, tweet.getPlaceURL());
                pstmt.setString(18, tweet.getPlaceGeometryType());
                pstmt.setString(19, tweet.getPlaceName());
                pstmt.setString(20, tweet.getPlaceFullName());
                pstmt.setString(21, tweet.getPlaceId());
                pstmt.setString(22, tweet.getSource());
                pstmt.setString(23, tweet.getJson());
                pstmt.setDate(24, tweet.getTimeOfCrawl());
                pstmt.setLong(25, tweet.getCrawledViaNewsMedia());
                
                pstmt.addBatch();  
            }  
            pstmt.executeBatch();  
            conn.commit();  
        } catch (SQLException e) {  
            try {  
                conn.rollback();  
            } catch (SQLException e1) {  
                e1.printStackTrace();  
            }  
            e.printStackTrace();  
        } finally {  
  
            try {  
                DBConn.close();  
            } catch (Exception e) {  
                e.printStackTrace();  
            }  
        }  
    }  
    
    public List<Tweet> selectAll(){
    	return select("select * from tweets_sample");
    }
    
    
    public List<Tweet> select(String sql) {
    	ResultSet rs = DBConn.exeR(sql);
		List<Tweet> tweets = new ArrayList<Tweet>();

		try {
			while(rs.next()){
				
				Tweet tweet = new Tweet();
				tweet.setId(rs.getLong(1));
				tweet.setUserId(rs.getInt(2));
				tweet.setUsername(rs.getString(3));
				tweet.setContent(rs.getString(4));
				tweet.setCreationTime(rs.getDate(5));
				tweet.setFavorite(rs.getInt(6));
				tweet.setReplyToPostId(rs.getLong(7));
				tweet.setReplyToUsername(rs.getString(8));
				tweet.setRetweetedFromPostId(rs.getLong(9));
				tweet.setReplyToUsername(rs.getString(10));
				tweet.setRetweetCount(rs.getInt(11));
				tweet.setLatitude(rs.getDouble(12));
				tweet.setLongitude(rs.getDouble(13));
				tweet.setPlaceCountry(rs.getString(14));
				tweet.setPlaceCountryCode(rs.getString(15));
				tweet.setPlaceStreetAddress(rs.getString(16));
				tweet.setPlaceURL(rs.getString(17));
				tweet.setPlaceGeometryType(rs.getString(18));
				tweet.setPlaceName(rs.getString(19));
				tweet.setPlaceFullName(rs.getString(20));
				tweet.setPlaceId(rs.getString(21));
				tweet.setSource(rs.getString(22));
				tweet.setJson(rs.getString(23));
				tweet.setTimeOfCrawl(rs.getDate(24));
				tweet.setCrawledViaNewsMedia(rs.getInt(25));
				tweets.add(tweet);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConn.close();
		}
		
		return tweets;

	}
}
