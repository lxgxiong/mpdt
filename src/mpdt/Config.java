package mpdt;

import java.io.IOException;
import java.util.Properties;

public class Config {
	//static静态成员声明Properties对象
	private static Properties prop = new Properties();
	//编写静态块加载prop里面属性文件
	static{
		try {
			System.out.println(prop.getClass().getResourceAsStream("db.properties")==null);
			System.out.println(Config.class.getResourceAsStream("db.properties")==null);
			prop.load(Config.class.getResourceAsStream("db.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//定义静态变量，赋值数据库连接信息
	public static final String DRIVER=prop.getProperty("DRIVER");
	public static final String URL = prop.getProperty("MYSQLURL");
	public static final String DBNAME =  prop.getProperty("DATABASENAME");
	public static final String DBUSERNAME =  prop.getProperty("DBUSERNAME");
	public static final String DBPASS =  prop.getProperty("DBPASS");
}